Disclaimer Module 6.x
--------------------------------------
The Disclaimer module is a very simple module
that affords site administrators the ability to add simple disclaimer text
to contents and force users/content creators to accept the sites terms-of-use of the web-site
before making postings.

It is ideal for web-sites that are fueled by user generated contents and does not usually require
users to signup.