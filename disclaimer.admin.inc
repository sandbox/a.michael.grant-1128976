<?php
/*entry point to the disclaimer settings page.*/
function list_content_types()
{
    drupal_set_title('Disclaimer');
    return drupal_get_form('list_form');
}

/*the disclaimer back end form*/
function list_form()
{
    $form = array();
    $form['dis_type_fieldset'] = array(
        '#type' => 'fieldset',
        '#title' => t('Content Types'),
    );
    $form['dis_type_fieldset']['DISCLAIMER_TYPES'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Content Types'),
      '#default_value' => variable_get('DISCLAIMER_TYPES', array()),
      '#options' => get_content_types(),
    );

    $form['dis_text'] = array(
        '#type' => 'fieldset',
        '#title' => t('Disclaimer Text'),
         '#description' => t('Select the content type to attach a disclaimer'),
    );
    $form['dis_text']['DISCLAIMER_TEXT'] = array(
      '#type' => 'textarea',
      '#title' => t(''),
      '#description' => t('Enter the Disclaimer text you want users to see when they try to create content '),
      '#default_value' => variable_get('DISCLAIMER_TEXT', ''),
    );

    return system_settings_form( $form );
}
/*getting a listing of all content types in the system*/
function get_content_types()
{
    $list =array();
    foreach( node_get_types() as $key => $type){
        $list[ $key ] = $type->name;
    }
    return $list;
}
?>